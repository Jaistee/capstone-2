<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'firstName' => '',
            'lastName' => '',
            'age' => 0,
            'address' => '',
            'email' => 'admin@admin.com',
            'username' => 'admin',
            'password' => 'admin12345',
            'role_id' => 1,
            'status_id' => 2,
            'equivalent_id' => 1,
            'img_path' => '',
        ]);
    }
}
