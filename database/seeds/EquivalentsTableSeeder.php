<?php

use Illuminate\Database\Seeder;

class EquivalentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('equivalents')->insert([[
            'bmiStatus' => 'Normal',
            'bpStatus' => 'Normal',
        ],[
            'bmiStatus' => 'Underweight',
            'bpStatus' => 'Low Blood',
        ],[
            'bmiStatus' => 'Overweight',
            'bpStatus' => 'High Blood',
        ],[
            'bmiStatus' => 'Obese',
            'bpStatus' => '',
        ]]);
    }
}
