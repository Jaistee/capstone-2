<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRolesToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->unsignedBigInteger('role_id')->default(2);
            $table->unsignedBigInteger('status_id')->default(1);
            $table->unsignedBigInteger('equivalent_id')->default(1);

            $table->foreign('role_id')
            ->on('roles')
            ->references('id')
            ->onDelete('restrict')
            ->onUpdate('cascade');

            $table->foreign('status_id')
            ->on('statuses')
            ->references('id')
            ->onDelete('restrict')
            ->onUpdate('cascade');

            $table->foreign('equivalent_id')
            ->on('equivalents')
            ->references('id')
            ->onDelete('restrict')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
