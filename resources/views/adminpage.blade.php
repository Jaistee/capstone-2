@extends('templates.template')
@section("title", "Add Item")
@section('content')


<div class="container">
		<div class="row">
			<div class="col-lg-12 py-5">
				<table class="table table-striped">
					<thead>
						<tr>
                            <th>Profile Picture</th>
							<th>First Name</th>
							<th>Last Name</th>
							<th>Age</th>
							<th>Address</th>
							<th>Email</th>
                            <th>Role</th>
                            <th>Status</th>
                            <th></th>
                            <th>     Actions</th>
						</tr>
				    </thead>
                    <tbody>
                     @foreach($users as $user)
                     @if(Auth::user()->id!=$user->id)
                        <tr>
                            <td><img src="{{asset($user->img_path)}}" alt="" height="300px"></td>
                            <td>{{$user->firstName}}</td>
                            <td>{{$user->lastName}}</td>
                            <td>{{$user->age}}</td>
                            <td>{{$user->address}}</td>
                            <td>{{$user->email}}</td>
                            <td>{{$user->role->name}}</td>
                            <td>{{$user->status->name}}</td>
                            <td>
                                @if($user->status_id != 2)
                                    <form action="/admin/markapproved/{{$user->id}}" method="POST">
                                     @csrf
                                        @method('PATCH')
                                         <button class="btn btn-warning">Approve</button>
                                    </form>
                                 @endif
                            </td>
                            <td>
                            @if($user->status_id != 2)
                                    <form action="/admin/deleteuser/{{$user->id}}" method="POST">
                                     @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger">Delete</button>
                                    </form> 
                                @endif        
                            
                             
                                @if($user->role_id != 1)
                                        <a href="/admin/viewuser/{{$user->id}}" class="btn btn-success">View Activity</button>
                                        @endif    
                             </td>
                        </tr>
                        @endif
                    @endforeach 
                    </tbody>
                 </table>
             </div>
         </div>
 </div>
@endsection