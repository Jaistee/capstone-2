@extends('templates.template')
@section("title", "Add Item")
@section('content')
	<h1 class="text-center py-5" >Add Vitals</h1>
	<div class="container">
		<div class="row">
			<div class="col-lg-6 offset-lg-3">
				<form action="/user/addvital" method="POST" enctype="multipart/form-data">
					@csrf
					<div class="form-group">
						<label for="weight">Weight:</label>
						<input type="number" name="weight" class="form-control">
					</div>
					<div class="form-group">
						<label for="height">Height:</label>
						<input type="number" name="height" class="form-control"></textarea>
					</div>
					<div class="form-group">
						<label for="systolic">Systolic:</label>
						<input type="number" name="systolic" class="form-control">
					</div>
					<div class="form-group">
						<label for="diastolic">Diastolic:</label>
						<input type="text" name="diastolic" class="form-control">
					</div>
					<div class="form-group">
						<label for="protein">Protein Intake:</label>
						<input type="number" name="protein" class="form-control" step="any">
					</div>
					<div class="form-group">
						<label for="fat">Fat Intake:</label>
						<input type="number" name="fat" class="form-control" step="any">
					</div>
					<div class="form-group">
						<label for="tdee">Total Daily Energy Expend:</label>
						<input type="number" name="tdee" class="form-control">
					</div>
					<div class="form-group">
                        <label for="date">Date Today:</label>
						<input type="date" name="date" class="form-control" value="{{date('Y-m-d')}}">
					</div>
					<button type="submit" class="btn btn-success">SUBMIT</button>
				</form>
			</div>
		</div>
	</div>

@endsection