@extends('templates.template')
@section("title", "Add Item")
@section('content')
<h1 class="text-center py-5" >Edit Profile</h1>
	<div class="container">
		<div class="row">
			<div class="col-lg-6 offset-lg-3">
				<form action="/user/editprofile/{{$user->id}}" method="POST" enctype="multipart/form-data">
					@csrf
                    @method('PATCH')
					<div class="form-group">
						<label for="firstname">First Name:</label>
						<input type="text" name="firstname" class="form-control" value="{{$user->firstName}}">
					</div>
					<div class="form-group">
						<label for="lastname">Last Name:</label>
						<input type="text" name="lastname" class="form-control" value="{{$user->lastName}}">
					</div>
					<div class="form-group">
						<label for="age">Age:</label>
						<input type="number" name="age" class="form-control" value="{{$user->age}}">
					</div>
					<div class="form-group">
						<label for="address">address:</label>
						<input type="text" name="address" class="form-control" value="{{$user->address}}">
					</div>
					<div class="form-group">
                        <label for="username">username:</label>
						<input type="text" name="username" class="form-control" value="{{$user->username}}">
					</div>
                 
					<button type="submit" class="btn btn-success">SAVE</button>
				</form>
			</div>
		</div>
	</div>
@endsection