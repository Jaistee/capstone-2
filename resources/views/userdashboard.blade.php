@extends('templates.template')
@section("title", "Add Item")
@section('content')
    <h1 class="text-center py-5">User Dashboard</h1>
    
              
    @foreach($users as $user)

     @if(Auth::user()->id==$user->id)
        <div class="offset-lg-4 col-lg-4 py-2 ">
                <div class="card">
               
                <img src="{{asset($user->img_path)}}" alt="" height="300px">
                <h5 class="card-title">First Name: {{$user->firstName}}</h5>
			 			<p class="card-text">Last Name: {{$user->lastName}}</p>
			 			<p class="card-text">username: {{$user->username}}</p>
                         <p class="card-text">Email Address: {{$user->email}}</p>
                         @auth
                         @if(Auth::user()->role_id==2)
                         <a class="btn btn-success" href="/user/editprofile/{{$user->id}}">Edit Profile</a>
                         @endif
                         @endauth
                </div>
        </div>
            @endif
        @endforeach 

        <div class="container col-lg-12  py-3">
        <div class="row">
        
        
        @foreach(Auth::user()->vitals as $vital)
       
            <div class="offset-lg-3 col-lg-6 py-2">
            
                <div class="card">
                 
                    <div class="card-body">  
                        <h3 class="card-text">Date Created: {{$vital->date}}</h3>
                        <p class="card-title">Weight: {{$vital->weight}}</p>
			 			<p class="card-text">Height: {{$vital->height}}</p>
			 			<p class="card-text">Sytolic Count: {{$vital->systolic}}</p>
                        <p class="card-text">Diastolic Count: {{$vital->diastolic}}</p>
                        <p class="card-text">Protein Intake: {{$vital->protein}}</p>
                        <p class="card-text">Fat Intake: {{$vital->fat}}</p>
                        <p class="card-text">TDEE: {{$vital->tdee}}</p>
                      
                @php
                    $current_user = Auth::user();
                    $result = ($vital->weight/($vital->height*$vital->height))*10000;
                    $pounds = $vital->weight/0.45;
                    $protein = round($vital->protein*$pounds);
                    $fat = round($vital->fat*$pounds);
                    $calDef = $vital->tdee - 300;
                    $proCal = $protein*4;
                    $fatCal = $fat*4;
                    $x = $calDef - $proCal -$fatCal;
                    $carbs = round($x/4);
                   
                    if ($result < 18.5) {
                        $current_user->equivalent_id = 2;
                    } else if ($result >= 18.5 && $result <=24.9) {
                        $current_user->equivalent_id = 1;
                    } else if ($result >= 25 && $result <=29.9) {
                        $current_user->equivalent_id = 3;
                    } else {
                        $current_user->equivalent_id = 4;
                    }

                    
                @endphp
                <p class="card-text">BMI Result: {{$result}} {{$current_user->equivalent->bmiStatus}}</p>
                <h3>Macro Nutrient</h3>
                <p class="card-text">Protein: {{$protein}}</p>
                <p class="card-text">Fat: {{$fat}}</p>
                <p class="card-text">Carbs: {{$carbs}}</p>
               
               
                <table>
                    <tbody>
                    @foreach($recoms as $recom)
                
                    @if($recom->protein == $protein && $recom->fat == $fat && $recom->carbs == $carbs)
                        <tr>
                        <td><img src="{{asset($recom->img_path)}}" alt="" height="300px"></td>
                                <td>{{$recom->name}}</td>
                                <td>{{$recom->description}}</td>
                                <td>{{$recom->protein}}</td>
                                <td>{{$recom->fat}}</td>
                                <td>{{$recom->carbs}}</td>
                        </tr>
                        @endif
                @endforeach
                    </tbody>
                </table>
               
                </div> 
            <br>
            
        @auth
        @if($current_user->role_id == 2)
            <form action="/user/savefindings/{{$vital->id}}" method="POST">
                @csrf
                @method('PATCH')
               
                <input name="status" type="hidden" value="{{$current_user->equivalent->bmiStatus}}">
                <input name="result" type="hidden" value="{{$result}}">
                <br>
                <button class="btn btn-danger" type="submit">Save</button>
            </form>
            <a class="btn btn-success" href="/user/editvitals/{{$vital->id}}">Edit</a>
            <form action="/user/deletevitals/{{$vital->id}}" method="POST">
                @csrf
                @method('DELETE')
                <button class="btn btn-warning" >Delete</button>
            </form>
        @endif
        @endauth
                </div>
            </div>
            @endforeach
        </div>
        </div>
    
@endsection