@extends('templates.template')
@section("title", "Add Item")
@section('content')
    <h1 class="text-center py-5">Admin Panel</h1>
    @foreach($recoms as $recom)
<div class="offset-lg-4 col-lg-4 py-2 ">
                <div class="card">
               
                <img src="{{asset($recom->img_path)}}" alt="" height="300px">
                <h5 class="card-title">Item Name: {{$recom->name}}</h5>
			 			<p class="card-text">Description: {{$recom->description}}</p>
			 			<p class="card-text">Fat Value: {{$recom->fat}}</p>
                         <p class="card-text">Protein Value: {{$recom->protein}}</p>
                         <p class="card-text">Carbs Value: {{$recom->carbs}}</p>
                         <a class="btn btn-success" href="/admin/edititem/{{$recom->id}}">Edit Item</a>
                         <form action="/admin/deleteitems/{{$recom->id}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-warning" >Delete</button>
                        </form>
                         
                </div>
        </div>
        @endforeach
@endsection