<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>@yield("title")</title>
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/cyborg/bootstrap.css">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="/catalog">Digital Health Center</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor03" aria-controls="navbarColor03" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarColor03">
    <ul class="navbar-nav mr-auto">
     
          @auth
            @if(Auth::user()->role_id==1)
            <li class="nav-item">
              <a class="nav-link" href="/admin">Users</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/admin/findings">Findings</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/addrecom">Add Food Recommendation</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/admin/item">Item</a>
            </li>
            @else
           
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/addvital">Add Vital</a>
            </li>
            @endif
            <li class="nav-item">
                <a class="nav-link" href="#">Hi {{Auth::user()->username}}</a>
            </li>
            <li class="nav-item">
              <form action="/logout" method="POST">
                @csrf
                <button type="submit" class="btn btn-secondary">Logout</button>
              </form>
            </li>
          @else
            <li class="nav-item">
              <a class="nav-link" href="/login">Login</a>
            </li>            <li class="nav-item">
              <a class="nav-link" href="/register">Register</a>
            </li>
          @endauth
        </ul>
      </div>
    </ul>
  </div>
</nav>

@yield("content")

<footer class="footer bg-light">
	<div class="container">
		<p class="text-center text-white">Created By Pogi</p>
	</div>
	
</footer>
</body>
</html>

