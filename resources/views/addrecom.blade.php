@extends('templates.template')
@section("title", "Add Item")
@section('content')
	<h1 class="text-center py-5" >Add Food</h1>
	<div class="container">
		<div class="row">
			<div class="col-lg-6 offset-lg-3">
				<form action="/admin/food" method="POST" enctype="multipart/form-data">
					@csrf
					<div class="form-group">
						<label for="name">Food Name:</label>
						<input type="text" name="name" class="form-control">
					</div>
					<div class="form-group">
						<label for="desc">Description:</label>
						<input type="text" name="desc" class="form-control"></textarea>
					</div>
					<div class="form-group">
						<label for="protein">Protein Value:</label>
						<input type="number" name="protein" class="form-control">
					</div>
					<div class="form-group">
						<label for="fat">Fat Value:</label>
						<input type="number" name="fat" class="form-control">
					</div>
					<div class="form-group">
                        <label for="carb">Carb Value:</label>
						<input type="number" name="carb" class="form-control" >
					</div>
                    <div>
                        <label for="img_path">Image: </label>
                        <input type="file" class="form-control" name="img_path">
                    </div>
					<button type="submit" class="btn btn-success">SUBMIT</button>
				</form>
			</div>
		</div>
	</div>

@endsection