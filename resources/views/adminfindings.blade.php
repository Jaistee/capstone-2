@extends('templates.template')
@section("title", "Add Item")
@section('content')


<div class="container">
		<div class="row">
			<div class="col-lg-12 py-5">
				<table class="table table-striped">
					<thead>
						<tr>
                            <th>Profile Picture</th>
							<th>First Name</th>
							<th>Username</th>
							<th>Email</th>
                            <th>Role</th>
                            <th>Status</th>
                    
                            <th>Weight</th>
                            <th>Height</th>
                            <th>Systolic</th>
                            <th>Diastolic</th>
                           
						</tr>
				    </thead>
                    <tbody>
                    @foreach($users as $user)
                       
                        @if(Auth::user()->id!=$user->id)
                            <tr>
                                <td><img src="{{asset($user->img_path)}}" alt="" height="300px"></td>
                                <td>{{$user->firstName}}</td>
                                <td>{{$user->username}}</td>
                                <td>{{$user->email}}</td>
                                <td>{{$user->role->name}}</td>
                                <td>{{$user->status->name}}</td>
                             <td>   
                             
                                <tr>
                                @foreach($vitals as $vital)
                                <td>{{$vital->weight}}</td>
                                <td>{{$vital->height}}</td>
                                <td>{{$vital->systolic}}</td>
                                <td>{{$vital->diastolic}}</td>
                                </tr>  
                            
                            </td>    
                            </tr>
                            @endforeach  
                            @endif
                    </tbody>
                   @endforeach  
                </table>
                
                    
             </div>
        </div>
    </div>            
@endsection