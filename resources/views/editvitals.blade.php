@extends('templates.template')
@section("title", "Add Item")
@section('content')
<h1 class="text-center py-5" >Edit Vitals</h1>
	<div class="container">
		<div class="row">
			<div class="col-lg-6 offset-lg-3">
				<form action="/user/editvitals/{{$vital->id}}" method="POST" enctype="multipart/form-data">
					@csrf
                    @method('PATCH')
					<div class="form-group">
						<label for="weight">weight:</label>
						<input type="number" name="weight" class="form-control" value="{{$vital->weight}}">
					</div>
					<div class="form-group">
						<label for="lastname">Height:</label>
						<input type="number" name="height" class="form-control" value="{{$vital->height}}">
					</div>
					<div class="form-group">
						<label for="systolic">Systolic:</label>
						<input type="number" name="systolic" class="form-control" value="{{$vital->systolic}}">
					</div>
					<div class="form-group">
						<label for="diastolic">Diastolic:</label>
						<input type="number" name="diastolic"  class="form-control" value="{{$vital->diastolic}}">
					</div>
					<div class="form-group">
						<label for="protein">Protein Intake:</label>
						<input type="number" name="protein" class="form-control" value="{{$vital->protein}}" step=any min=0 max=100>
					</div>
					<div class="form-group">
						<label for="fat">Fat Intake:</label>
						<input type="number" name="fat" class="form-control" value="{{$vital->fat}}">
					</div>
					<div class="form-group">
						<label for="tdee">Total Daily Energy Expend:</label>
						<input type="number" name="tdee" class="form-control" value="{{$vital->tdee}}">
					</div>
					<div class="form-group">
                        <label for="date">Date:</label>
						<input type="date" name="date" class="form-control" value="{{$vital->date}}">
					</div>
                    
					<button type="submit" class="btn btn-success">SAVE</button>
				</form>
			</div>
		</div>
	</div>
@endsection