@extends('templates.template')
@section("title", "Add Item")
@section('content')
<h1 class="text-center py-5" >Edit Item</h1>
	<div class="container">
		<div class="row">
			<div class="col-lg-6 offset-lg-3">
				<form action="/admin/edititem/{{$recom->id}}" method="POST" enctype="multipart/form-data">
					@csrf
                    @method('PATCH')
					<div class="form-group">
						<label for="name">Item Name:</label>
						<input type="text" name="name" class="form-control" value="{{$recom->name}}">
					</div>
					<div class="form-group">
						<label for="desc">Description:</label>
						<input type="text" name="desc" class="form-control" value="{{$recom->description}}">
					</div>
					<div class="form-group">
						<label for="fat">Fat Value:</label>
						<input type="number" name="fat" class="form-control" value="{{$recom->fat}}">
					</div>
					<div class="form-group">
						<label for="protein">Protein Value:</label>
						<input type="number" name="protein"  class="form-control" value="{{$recom->protein}}">
					</div>
					<div class="form-group">
                        <label for="carb">Carb Value:</label>
						<input type="number" name="carb" class="form-control" value="{{$recom->carbs}}">
					</div>
                    <div class="form-group">
						<label for="img_path">Image:</label>
						<input type="file" name="img_path" class="form-control" value="{{$recom->img_path}}">
               		 </div>
					<button type="submit" class="btn btn-success">SAVE</button>
				</form>
			</div>
		</div>
	</div>
@endsection