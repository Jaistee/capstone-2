<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'HomeController@index');
Route::get('/addvital', 'VitalController@create');


Route::post('/user/addvital', 'VitalController@store'); 
Route::get('/user/dashboard', 'VitalController@index');
Route::get('/user/editprofile/{id}', 'VitalController@edit');
Route::patch('/user/editprofile/{id}', 'VitalController@update');

Route::get('/user/editvitals/{id}', 'VitalController@editVital');
Route::patch('/user/editvitals/{id}', 'VitalController@updateVital');
Route::delete('/user/deletevitals/{id}', 'VitalController@deleteVital');

Route::patch('/user/savefindings/{id}', 'VitalController@storeFindings');

Route::get('/admin','UserController@index');
Route::get('/admin/findings', 'UserController@showFindings');
Route::patch('/admin/markapproved/{id}', 'UserController@markApproved');
Route::delete('/admin/deleteuser/{id}', 'UserController@deleteUser');
Route::get('/admin/viewuser/{id}', 'UserController@viewUser');
Route::get('/addrecom', 'UserController@create');
Route::post('/admin/food', 'UserController@foodRecom');
Route::get('/admin/item', 'UserController@showItem');
Route::get('/admin/edititem/{id}', 'UserController@editItem');
Route::patch('/admin/edititem/{id}', 'UserController@updateItem');
Route::delete('/admin/deleteitems/{id}', 'UserController@deleteItem');
Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
