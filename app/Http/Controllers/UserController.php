<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Vital;
use App\Recommendation;
class UserController extends Controller
{
 
    public function index()
    {
        $users = User::all();
        $vitals = Vital::all();
        $recoms = Recommendation::all();
        return view('adminpage', compact('users','vitals', 'recoms'));
    }

    public function markApproved($id)
    {
        $users = User::find($id);
        $users->status_id = 2;
        $users->save();
        return redirect('/admin');
    }

    public function deleteUser($id)
    {
        $users = User::find($id);
        $users->delete();
        return redirect('/admin');
    }
  
    public function viewUser($id)
    {
        $users = User::find($id);
        return redirect('/user/dashboard');
    }
    public function showFindings()
    {
        $users = User::all();
        $vitals = Vital::all();
        return view('adminfindings', compact('users','vitals'));
        
    }

    public function foodRecom(Request $request)
    {
        // $rules = array(
        //     "name" => "required",
        //     "description" => "required",
        //     "protein" => "required|numeric",
        //     "fat" => "required|numeric",
        //     "carb" => "required|numeric",
        //     "img_path" => "required"
        // );

        // $this->validate($request, $rules);

        $new_recom = new Recommendation;
        $new_recom->name = $request->name;
        $new_recom->description = $request->desc;
        $new_recom->protein = $request->protein;
        $new_recom->fat = $request->fat;
        $new_recom->carbs = $request->carb;
        
        if($request->file('img_path') != null){
            $image = $request->file('img_path');
            $image_name = time().".".$image->getClientOriginalExtension();
            $destination = "images/";
            $image->move($destination, $image_name);
            $new_recom->img_path = $destination.$image_name;

        }
       
        $new_recom->save();
        return redirect('/admin');
    }
    public function create()
    {
        return view('addrecom');
    }

    public function showItem()
    {
        $recoms = Recommendation::all();
        return view('admin', compact('recoms'));

    }
    public function editItem($id)
    {
        $recom = Recommendation::find($id);
        return view('adminupdateitem', compact('recom'));
    }

    public function updateItem(Request $request, $id)
    {
        $recoms = Recommendation::find($id);
      

        $recoms->name = $request->name;
        $recoms->description = $request->desc;
        $recoms->protein = $request->protein;
        $recoms->fat = $request->fat;
        $recoms->carbs = $request->carb;

        if($request->file('img_path') != null){
            $image = $request->file('img_path');
            $image_name = time().".".$image->getClientOriginalExtension();
            $destination = "images/";
            $image->move($destination, $image_name);
            $recoms->img_path = $destination.$image_name;

        }
        $recoms->save();
    
        return redirect('/admin');
    }

    public function deleteItem(Request $request, $id)
    {
        $recoms = Recommendation::find($id);
        $recoms->delete();
      
        return redirect('/admin/item');
    }
}
