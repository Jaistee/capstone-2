<?php

namespace App\Http\Controllers;
use App\Vital;
use App\User;
use App\Equivalent;
use App\Recommendation;
use Auth;
use Illuminate\Http\Request;


class VitalController extends Controller
{
    public function index()
    {

        $vitals = Vital::all();
        $users =  User::all();
        $equivalents = Equivalent::all();
        $recoms = Recommendation::all();
        return view('userdashboard', compact('vitals', 'users','equivalents','recoms'));
    }

    public function create()
    {
        return view('addvitalform');
    }

    public function store(Request $request)
    {
        $rules = array(
            "weight" => "required|numeric",
            "height" => "required|numeric",
            "systolic" => "required|numeric",
            "diastolic" => "required|numeric",
            "protein" => "required|numeric",
            "fat" => "required|numeric",
            "tdee" => "required|numeric",
            "date" => "required"
        );

        $this->validate($request, $rules);

        $new_vital = new Vital;
        $new_vital->weight = $request->weight;
        $new_vital->height = $request->height;
        $new_vital->systolic = $request->systolic;
        $new_vital->diastolic = $request->diastolic;
        $new_vital->protein = $request->protein;
        $new_vital->fat = $request->fat;
        $new_vital->tdee = $request->tdee;
        $new_vital->date = $request->date;
        $new_vital->save();

        $user = Auth::user();
        $user->vitals()->attach($new_vital->id, [
             "bmiResult" => null,
             "bpResult" => null,
        ]);
    
        return redirect('/user/dashboard');
    }

    public function edit($id)
    {
        $user = User::find($id);
        return view('edituserprofile', compact('user'));
    }

    public function update(Request $request, $id)
    {
        $users = User::find($id);
      
        // $rules = array(
        //     "firstName" => "required",
        //     "lastName" => "required",
        //     "age" => "required|numeric",
        //     "address" => "required",
        //     "username" => "required",
        //     "password" => "required"
        // );

        // $this->validate($request, $rules);

        $users->firstName = $request->firstname;
        $users->lastName = $request->lastname;
        $users->age = $request->age;
        $users->address = $request->address;
        $users->username = $request->username;
        
        $users->save();

   

        return redirect('user/dashboard');
    }
    
    
    public function editVital($id)
    {
        $vital = Vital::find($id);
        return view('editvitals', compact('vital'));
    }

    public function updateVital(Request $request, $id)
    {
        $vitals = Vital::find($id);
        $rules = array(
            "weight" => "required|numeric",
            "height" => "required|numeric",
            "systolic" => "required|numeric",
            "diastolic" => "required|numeric",
            "protein" => "required|numeric",
            "fat" => "required|numeric",
            "tdee" => "required|numeric",
            "date" => "required"
        );

        $this->validate($request, $rules);

        $vitals->weight = $request->weight;
        $vitals->height = $request->height;
        $vitals->systolic = $request->systolic;
        $vitals->diastolic = $request->diastolic;
        $vitals->protein = $request->protein;
        $vitals->fat = $request->fat;
        $vitals->tdee = $request->tdee;
        $vitals->date = $request->date;
       
        $vitals->save();
    
        return redirect('/user/dashboard');
    }

    public function deleteVital(Request $request, $id)
    {
        $vitals = Vital::find($id);
        $vitals->delete();
      
        return redirect('/user/dashboard');
    }
    
    public function storeFindings(Request $request, $vital_id)
    {
        $user = Auth::user();
        $user->vitals()->detach($vital_id);
        $user->vitals()->attach($vital_id, [
             "bmiResult" => $request->status,
             "bpResult" => $request->result,
        ]);

        return redirect('/user/dashboard');
    
    }
}
