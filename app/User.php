<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstName', 'lastName','age','address','email','username', 'password','img_path'
        
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function role(){
        return $this->belongsTo('App\Role');
    }
    
    public function status(){
        return $this->belongsTo('App\Status');
    }
    

    public function equivalent(){
        return $this->belongsTo('App\Equivalent');
    }

    public function vitals(){
    	return $this->belongsToMany('\App\Vital')->withPivot("bmiResult", "bpResult")->withTimestamps();
    }
}
