<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vital extends Model
{
    public function users(){
    	return $this->belongsToMany('\App\User')->withPivot("bmiResult", "bpResult")->withTimestamps();
    }
}
